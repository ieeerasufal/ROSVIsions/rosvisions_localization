#!/usr/bin/env python
import cv2
import numpy as np
import rospy
from rosvisions_localization.msg import ArPose
import geometry_msgs.msg
import tf2_ros

cameraMatrix = np.array([[624.8946224885802, 0, 296.1545738663063],
                                [0, 625.7595662823172, 262.0434043718105],
                                [0, 0, 1]])
distCoeffs = np.array([-0.0622267965661656, 0.005629446814725758, -0.001019593039192263, 0.0008802374196681531, 0])

markerLength = 20

dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)

cap = cv2.VideoCapture(0)
idis = np.empty(1, dtype='int')
pub = rospy.Publisher('detected_tags', ArPose, queue_size=10)
rospy.init_node('aruco_detect', anonymous=False)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    
    # Comeca a deteccao dos marcadores
    res = cv2.aruco.detectMarkers(gray,dictionary)
    
    # Caso algum marcador seja detectado
    if len(res[0]) > 0:
        print('######')

        for i in range(len(res[1])):
            ## Para pegar o id do marcador: res[1][i]

            detected_ar = list()
            ret = cv2.aruco.estimatePoseSingleMarkers([res[0][i]], markerLength, cameraMatrix, distCoeffs)

                #rotation = ret[0][0][0]
                #translation = ret[1][0][0]
                #rotationX = ret[0][0][0][0]
                #rotationY = ret[0][0][0][1]
                #rotationZ = ret[0][0][0][2]
                #translationX = ret[1][0][0][0]
                #translationY = ret[1][0][0][1]
                #translationZ = ret[1][0][0][2]
                
            obj_dict = {'Id': res[1][i], 'Rotation': ret[0][0][0], 'Translaction': ret[1][0][0]}
            detected_ar.append(obj_dict)

            imgWithAruco = cv2.aruco.drawDetectedMarkers(gray, [res[0][i]], res[1][i], (0, 255, 0))
            imgWithAruco = cv2.aruco.drawAxis(imgWithAruco, cameraMatrix, distCoeffs, ret[0], ret[1], 3)

            for detected_tag in detected_ar:
                msg_vector = ArPose()
                msg_vector.id = detected_tag['Id'][0]
                msg_vector.rvec = detected_tag['Rotation']
                msg_vector.tvec = detected_tag['Translaction']  
                rospy.loginfo(msg_vector)
                pub.publish(msg_vector)
            
            #handle_pose(detected_ar)
    
    # Display the resulting frame
    cv2.imshow('frame',gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
