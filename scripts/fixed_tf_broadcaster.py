#!/usr/bin/env python  
import rospy
import tf
import tf2_ros
import tf2_msgs.msg
import geometry_msgs.msg
import numpy as np
from rosvisions_localization.msg import ArPose 

pub_tf = rospy.Publisher("/tf", tf2_msgs.msg.TFMessage, queue_size=1)

def handle_pose(pose_msg):
    if(pose_msg.id) == 0:
        tag_id = "aruco_0"
        br = tf2_ros.TransformBroadcaster()
        t = geometry_msgs.msg.TransformStamped()

        theta = np.linalg.norm(pose_msg.rvec)
        xr = pose_msg.rvec[0]/theta
        yr = pose_msg.rvec[1]/theta
        zr = pose_msg.rvec[2]/theta

        t.header.stamp = rospy.Time.now()
        t.header.frame_id = "aruco_1"
        t.child_frame_id = tag_id
        t.transform.translation.x = pose_msg.tvec[0]
        t.transform.translation.y = pose_msg.tvec[1]
        t.transform.translation.z = 0#pose_msg.tvec[2]
        q = tf.transformations.quaternion_from_euler(xr, yr, zr, 'rxyz')
        t.transform.rotation.x = q[0]
        t.transform.rotation.y = q[1]
        t.transform.rotation.z = q[2]
        t.transform.rotation.w = q[3]

        tfm = tf2_msgs.msg.TFMessage([t])
	pub_tf.publish(tfm)

if __name__ == '__main__':
    print("Starting pose broadcaster...")
    rospy.init_node('fixed_tf2_broadcaster', anonymous=False)
    rospy.Subscriber('detected_tags', ArPose, handle_pose)
    rospy.spin()
